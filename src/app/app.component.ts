import { Component } from '@angular/core';
import {ChatService} from './chat.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  constructor(private chatService: ChatService) {
    chatService.messages.subscribe(msg => {
      console.log('Response From Websocket Server: ' + msg);
    });
  }

  private message = {
    author: 'elliot forbes',
    message: 'howdy folks'
  };

  sendMsg() {
    console.log('New Message Sent From Client');
    this.chatService.messages.next(this.message);
  }

}

